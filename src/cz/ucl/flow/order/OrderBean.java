package cz.ucl.flow.order;

/**
 * Created by hideo on 12.04.16.
 */
import cz.ucl.fa.model.Customer;
import cz.ucl.fa.model.Holiday;
import cz.ucl.fa.model.Traveller;
import cz.ucl.fa.model.util.JPAUtil;
import cz.ucl.jsf.controller.PurchaseController;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.event.ValueChangeEvent;
import javax.faces.flow.FlowScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@FlowScoped("order")
public class OrderBean implements Serializable {

    @PostConstruct
    public void init() {
        System.out.println("!!! Entered Order flow " + this.toString());
    }

    @PreDestroy
    public void destroy() {
        System.out.print("!!! Leaving Order flow " + this.toString());
    }

    @Inject
    private PurchaseController purchaseController;

    public long getSelectedTripId() {
        return selectedTripId;
    }

    public void setSelectedTripId(long selectedTripId) {
        this.selectedTripId = selectedTripId;
    }

    private long selectedTripId = -1;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    private Customer customer;

    private Holiday selectedHoliday;

    public Holiday getSelectedHoliday() {
        setSelectedHoliday(selectedTripId);
        System.out.println("holiday je " + this.selectedHoliday + "id" + selectedTripId);
        return selectedHoliday;
    }
    public void setSelectedHoliday(long selectedTripId) {
        System.out.println("selectedTripId je " + selectedTripId);
        if (selectedTripId != -1) {
            EntityManager em = JPAUtil.createEntityManager();
            selectedHoliday = em.find(Holiday.class, selectedTripId);
            selectedHoliday.getAccommodation().size();
            selectedHoliday.getTransportation().size();
            selectedHoliday.getServices().size();
            em.close();
        }
    }

    public void tripChanged(ValueChangeEvent e) {
        if (null != e.getNewValue() ) {
            selectedTripId = ((Number) e.getNewValue()).longValue();
            System.out.println("\ttripChanged fired with " + selectedTripId);
        }
    }

    private String firstName;
    private String surname;
    private String ccNumber;
    private String ccValidity;
    private String travellerIdNumber;
    private String travellerFirstName;
    private String travellerSurname;
    private List<Traveller> travellers = new ArrayList<>();
    private Traveller traveller;

    public List<Traveller> getTravellers() {
        return travellers;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        travellerFirstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
        travellerSurname = surname;
    }

    public String removeTraveller(Traveller traveller) {
        travellers.remove(traveller);
        return null;
    }


    public String getCcValidity() {
        return ccValidity;
    }

    public void setCcValidity(String ccValidity) {
        this.ccValidity = ccValidity;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(String ccNumber) {
        this.ccNumber = ccNumber;
    }


    public String getTravellerFirstName() {
        return travellerFirstName;
    }

    public void setTravellerFirstName(String travellerFirstName) {
        this.travellerFirstName = travellerFirstName;
    }

    public String getTravellerSurname() {
        return travellerSurname;
    }

    public void setTravellerSurname(String travellerSurname) {
        this.travellerSurname = travellerSurname;
    }

    public String getTravellerIdNumber() {
        return travellerIdNumber;
    }

    public void setTravellerIdNumber(String travellerIdNumber) {
        this.travellerIdNumber = travellerIdNumber;
    }


    public void addTraveller() {
        Traveller traveller = new Traveller();
        traveller.setFirstName(travellerFirstName);
        traveller.setSurname(travellerSurname);
        traveller.setIdNumber(travellerIdNumber);
        travellers.add(traveller);
        travellerIdNumber="";
        travellerFirstName="";
        travellerSurname="";
    }



    public String getReturnValue() {
        return "/index.jsf";
    }

}
