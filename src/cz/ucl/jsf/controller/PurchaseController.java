package cz.ucl.jsf.controller;

import java.io.Serializable;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.flow.FlowScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.validation.constraints.Pattern;

import com.sun.istack.internal.NotNull;
import cz.ucl.fa.model.Contract;
import cz.ucl.fa.model.CreditCard;
import cz.ucl.fa.model.Customer;
import cz.ucl.fa.model.Holiday;
import cz.ucl.fa.model.Traveller;
import cz.ucl.fa.model.util.JPAUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

@ManagedBean(name = "purchaseController")
@Named
@FlowScoped("order")
public class PurchaseController implements Serializable {
    /**
     * Vznikající objekt smlouvy
     */
    private Contract contract = new Contract();

    /**
     * Pomocné objekty pro přidávání cestujících do smlouvy
     */
    private Traveller currentTraveller = new Traveller();
    private Integer selectedTravellerPos;
    private List<Traveller> travellers = new ArrayList<>();

    /**
     * Proměnné pro práci s existujícími zákazníky
     */
    private boolean existingCustomer;
    private Long existingCustomerId;

    /**
     * Asociativní pole Název->ID všech zájezdů
     */
    private Map<String, Long> allTrips;

    /**
     * Pomocná pole pro ukládání čísla a platnosti kreditní karty
     */
    private String[] creditCardNo = new String[4];
    private String[] creditCardValidity = new String[2];

    /**
     * Vyhledá všechny katalogové zájezdy a naplní je do asociativního pole
     * allTrips (v podobě vhodné k použití pro f:selectItems)
     */
    @SuppressWarnings("unchecked")
    public Map<String, Long> getAllTrips() {
        if (allTrips == null) {
            allTrips = new TreeMap<String, Long>();
            allTrips.put("", -1L);

            EntityManager em = JPAUtil.createEntityManager();
            List<Object[]> queryResult = em.createNamedQuery(
                    "Trip.getAllNamesAndIds").getResultList();
            for (Object[] item : queryResult) {
                allTrips.put((String) item[0], (Long) item[1]);
            }
        }
        return allTrips;
    }

    private Holiday selectedHoliday;

    public Holiday getSelectedHoliday() {
        return selectedHoliday;
    }

    private long selectedTripId = -1;

    public long getSelectedTripId() {
        return selectedTripId;
    }

    /**
     * Na základě nastavení identifikátoru zájezdu (bude voláno JSF implementací poté,
     * co uživatel vybere ze seznamu) z databáze načte objekt typu Holiday a naplní do
     * vznikajícího objektu smlouvy (contract)
     */
    public void setSelectedTripId(long selectedTripId) {
        if (selectedTripId != -1) {
            this.selectedTripId = selectedTripId;
            EntityManager em = JPAUtil.createEntityManager();
            selectedHoliday = em.find(Holiday.class, selectedTripId);
            selectedHoliday.getAccommodation().size();
            selectedHoliday.getTransportation().size();
            selectedHoliday.getServices().size();
            contract.setHoliday(selectedHoliday);
            em.close();
        }
    }

    public void tripChanged(ValueChangeEvent e) {
        if (null != e.getNewValue()) {
            setSelectedTripId(((Number) e.getNewValue()).longValue());
        }
    }


    private Customer currentCustomer = new Customer();

    public String getCurrentCustomerFirstName() {
        return currentCustomer.getFirstName();
    }

    public void setCurrentCustomerFirstName(String firstName) {
        currentCustomer.setFirstName(firstName);
        currentTraveller.setFirstName(firstName);
    }

    public String getCurrentCustomerSurname() {
        return currentCustomer.getSurname();
    }

    @SuppressWarnings("unchecked")
    public void setCurrentCustomerSurname(String surname) {
        currentCustomer.setSurname(surname);
        currentTraveller.setSurname(surname);
        if (knownCustomer(currentCustomer)) {
            existingCustomer = true;
            EntityManager em = JPAUtil.createEntityManager();
            Session session = em.unwrap(Session.class);
            Criteria cr = session.createCriteria(Customer.class);
            List<Customer> results = cr.list();
            em.close();
            existingCustomerId = results.get(0).getId();
        } else {
            existingCustomerId = null;
            existingCustomer = false;
        }
        contract.setCustomer(currentCustomer);
    }

    @SuppressWarnings("unchecked")
    private boolean knownCustomer(Customer customer) {
        EntityManager em = JPAUtil.createEntityManager();
        Session session = em.unwrap(Session.class);
        Criteria cr = session.createCriteria(Customer.class);
        cr.add(Restrictions.eq("firstName",customer.getFirstName()));
        cr.add(Restrictions.eq("surname",customer.getSurname()));
        List<Customer> results = cr.list();
        em.close();
        return !results.isEmpty();
    }


    @Pattern(regexp = "(\\d{4}([- ])?){3}\\d{4}", message = "16 numbers")
    @NotNull
    private String ccNumber;

    public String getCcNumber() {
        return ccNumber;
    }

    /**
     * Sets ccNumber[] according to provided parameter.
     *
     * @param ccNumber is expected to be in form
     *                 xxxx-xxxx-xxxx-xxxx or
     *                 xxxx xxxx xxxx xxxx or
     *                 xxxxxxxxxxxxxxxx
     */
    public void setCcNumber(String ccNumber) {
        String number = ccNumber.replace("- ", "");
        StringBuilder s = new StringBuilder(number).insert(4, "-").insert(9, "-").insert(14, "-");
        creditCardNo = s.toString().split("-");
        this.ccNumber = ccNumber;
    }

    @Pattern(regexp = "\\d{2}(/)?\\d{2}", message = "mm/yy")
    @NotNull
    private String ccValidity;

    public String getCcValidity() {
        return ccValidity;
    }

    /**
     * Sets ccValidity[] according to provided parameter.
     *
     * @param ccValidity is expected to be in form of <tt>mm/yy</tt> or <tt>mmyy<tt/>.
     */
    public void setCcValidity(String ccValidity) {
        String number = ccValidity.replace("/", "");
        StringBuilder s = new StringBuilder(number).insert(2, "/");
        creditCardValidity = s.toString().split("/");
        this.ccValidity = ccValidity;
    }


    /**
     * Pro existujícího zákazníka zkopíruje jméno/příjmení z databáze pro nastavení
     * hodnot do objektu currentTraveller.
     */
    public void copyTravellerFromCustomer() {
        currentTraveller = new Traveller();
        if (existingCustomer && existingCustomerId != null) {
            EntityManager em = JPAUtil.createEntityManager();
            Customer existingCustomerValue = (Customer) em.find(Customer.class,
                    existingCustomerId);
            currentTraveller.setSurname(existingCustomerValue.getSurname());
            currentTraveller.setFirstName(existingCustomerValue.getFirstName());
            em.close();
        } else {
            currentTraveller.setSurname(contract.getCustomer().getSurname());
            currentTraveller
                    .setFirstName(contract.getCustomer().getFirstName());
        }

    }

    @SuppressWarnings("unchecked")
    private Traveller knownTraveller(String id) {
        EntityManager em = JPAUtil.createEntityManager();
        Session session = em.unwrap(Session.class);
        Criteria cr = session.createCriteria(Traveller.class);
        cr.add(Restrictions.eq("idNumber",id));
        List<Traveller> results = cr.list();
        em.close();
        if (results.isEmpty()) return currentTraveller;
        else return results.get(0);
    }

    public String getTravellerIdNumber() {
        return currentTraveller.getIdNumber();
    }

    public void setTravellerIdNumber(String idNumber) {
        currentTraveller = knownTraveller(idNumber);
        currentTraveller.setIdNumber(idNumber);
    }

    public String getTravellerFirstName() {
        return currentTraveller.getFirstName();
    }

    public void setTravellerFirstName(String firstName) {
        currentTraveller.setFirstName(firstName);
    }

    public String getTravellerSurname() {
        return currentTraveller.getSurname();
    }

    public void setTravellerSurname(String surname) {
        currentTraveller.setSurname(surname);
    }

    public void addTraveller() {
        travellers.add(currentTraveller);
        currentTraveller = new Traveller();
    }

    public void removeTraveller(Traveller t) {
        travellers.remove(t);
    }

    public List<Traveller> getTravellers() {
        return travellers;
    }

    /**
     * Provede potvrzení smlouvy a její zápis do databáze. Rozlišuje mezi existujícím
     * a novým zákazníkem. Neobsahuje (zatím) validace
     */
    public String confirmContract() {
        EntityManager em = JPAUtil.createEntityManager();
        if (existingCustomer) {
            if (existingCustomerId == null) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage("No customer selected",
                                "No existing customer was selected"));
                return "validationError";
            } else {
                contract.setCustomer((Customer) em.find(Customer.class,
                        existingCustomerId));
            }
        } else {
            CreditCard card = new CreditCard();
            card.setNumber(creditCardNo[0] + creditCardNo[1] + creditCardNo[2]
                    + creditCardNo[3]);

            card.setValidity(creditCardValidity[0] + creditCardValidity[1]);
            card.setOwnerName(contract.getCustomer().getName());
            contract.getCustomer().setCard(card);
        }

        em.getTransaction().begin();
        contract.setHoliday(em.merge(contract.getHoliday()));
        em.persist(contract);
        em.getTransaction().commit();

        return "confirmed";
    }

    public String getReturnValue() {
        return "/index.jsf";
    }

    @PostConstruct
    public void init() {
        System.out.println("!!! PurchaseController entered Order flow " + this.toString());
    }

    @PreDestroy
    public void destroy() {
        System.out.print("!!! PurchaseController leaving Order flow " + this.toString());
    }

}
